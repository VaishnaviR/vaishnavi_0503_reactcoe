import React from "react";
import "./App.css";
import FundCategory from "./FundCategory.js";
const treeStructure = [
  {
    name: "Marketing & Sales",
    percentage: 50,
    amount: 0,
    children: [
      {
        name: "Marketing",
        percentage: 25,
        amount: 0,
        children: []
      },
      {
        name: "Sales",
        percentage: 25,
        amount: 0,
        children: []
      }
    ]
  },
  {
    name: "Finance",
    percentage: 25,
    amount: 0,
    children: [
      {
        name: "Inhouse Expenses",
        percentage: 15,
        amount: 0,
        children: []
      },
      {
        name: "Tax Audit",
        percentage: 10,
        amount: 0,
        children: []
      }
    ]
  },
  {
    name: "Operation & Maintanence",
    percentage: 25,
    amount: 0,
    children: [
      {
        name: "Operation",
        percentage: 13,
        amount: 0,
        children: []
      },
      {
        name: "Maintanernce",
        percentage: 12,
        amount: 0,
        children: []
      }
    ]
  }
];
class FundAllocation extends React.Component {
  constructor(props) {
    super(props);
    this.state = { allocatedAmount: 0, treeData: treeStructure };
    this.handleChange = this.handleChange.bind(this);
    this.onChangeOfPercentValue = this.onChangeOfPercentValue.bind(this);
    this.iterateTheDataStructure = this.iterateTheDataStructure.bind(this);
    this.childrenPercentageValue = this.childrenPercentageValue.bind(this);
  }
  handleChange = event => {
    if (event.target.value < 0) event.target.value = 0;
    this.setState({ allocatedAmount: event.target.value });
  };
  childrenPercentageValue = (
    data,
    newParentPercentage,
    oldParentPercentage,
    changeState = "parent",
    childIndex = -1
  ) => {
    let finalData = [];
    debugger;
    for (let i in data) {
      let temp = data[i];
      let oldValue = temp.percentage;
      if (newParentPercentage === 0 && changeState !== "child")
        temp.percentage = 0;
      else if (changeState === "parent") {
        if (oldParentPercentage === 0) {
          temp.percentage = newParentPercentage / 2;
        } else {
          temp.percentage =
            newParentPercentage * (temp.percentage / oldParentPercentage);
        }
      } else if (changeState === "child" && i === childIndex)
        temp.percentage = newParentPercentage;
      else if (changeState === "child" && i !== childIndex)
        temp.percentage = temp.percentage;
      if (temp.children.length > 0) {
        temp.children = this.childrenPercentageValue(
          temp.children,
          temp.percentage,
          oldValue
        );
      }
      finalData.push(temp);
    }
    return finalData;
  };
  onChangeOfPercentValue = (value, parentIndex, childIndex) => {
    debugger;
    console.log(value);
    if (value === null || value === "" || value < 0 || value === ".00")
      value = 0;
    let data = this.state.treeData;
    let finalData = [];
    if (parseInt(value) < 0) value = 0;
    if (value > 100) value = 100;
    let percentageRemaining = 0;
    let totalParentPercentage = 0;
    if (parentIndex === -1) {
      percentageRemaining = 100 - parseInt(value);
    } else {
      let parentData = data[parentIndex];

      for (let i in parentData.children) {
        if (i !== childIndex)
          totalParentPercentage += parentData.children[i].percentage;
        else totalParentPercentage += parseInt(value);
      }
      percentageRemaining = 100 - totalParentPercentage;
    }
    if (percentageRemaining >= 0) {
      let totalPercentage = 0;
      for (let i in data) {
        if (i !== childIndex) {
          totalPercentage += data[i].percentage;
        }
      }
      for (let i in data) {
        let temp = data[i];
        if (parentIndex !== -1 && i === parentIndex) {
          let oldValue = temp.percentage;
          debugger;
          temp.percentage =
            totalParentPercentage === "" ? 0 : parseInt(totalParentPercentage);
          if (temp.children.length > 0) {
            temp.children = this.childrenPercentageValue(
              temp.children,
              value,
              oldValue,
              "child",
              childIndex
            );
          }
          finalData.push(temp);
        } else if (i !== childIndex) {
          let oldValue = temp.percentage;
          temp.percentage = parseInt(
            percentageRemaining * (oldValue / totalPercentage)
          );
          if (temp.children.length > 0) {
            temp.children = this.childrenPercentageValue(
              temp.children,
              temp.percentage === "" ? 0 : temp.percentage,
              oldValue
            );
          }
          finalData.push(temp);
        } else if (i === childIndex) {
          let oldValue = temp.percentage;
          temp.percentage = parseInt(value);
          if (temp.children.length > 0) {
            temp.children = this.childrenPercentageValue(
              temp.children,
              temp.percentage === "" ? 0 : temp.percentage,
              oldValue
            );
            finalData.push(temp);
          }
        }
      }
    }

    this.setState({ treeData: finalData });
    this.iterateTheDataStructure(finalData, this.state.allocatedAmount, 100);
  };

  iterateTheDataStructure = (treeData, allocatedAmount, totalPercentage) => {
    this.setState({
      treeData: this.calculateAllocatedAmouts(
        treeData,
        allocatedAmount,
        totalPercentage
      )
    });
    console.log(this.state.treeData);
  };
  calculateAllocatedAmouts = (treeData, allocatedAmount, totalPercentage) => {
    let finalData = [];
    treeData.forEach(d => {
      let temp = d;
      if (totalPercentage === 0) {
        temp.amount = 0;
      } else {
        temp.amount = allocatedAmount * (temp.percentage / totalPercentage);
      }

      if (temp.children.length > 0) {
        this.calculateAllocatedAmouts(
          temp.children,
          temp.amount,
          temp.percentage
        );
      }
      finalData.push(temp);
    });
    return finalData;
  };
  render() {
    return (
      <div className="container">
        <h3 textAlign="center">Fund Allocation</h3>
        <div className="row-flex flex-justify header">
          <p>Enter Allocated Total Amount</p>
          <input
            type="number"
            value={this.state.allocatedAmount}
            onChange={this.handleChange}
          />
          <button
            onClick={() =>
              this.iterateTheDataStructure(
                this.state.treeData,
                this.state.allocatedAmount,
                100
              )
            }
          >
            Submit
          </button>
        </div>
        <FundCategory
          treeData={this.state.treeData}
          onPercentageChange={this.onChangeOfPercentValue}
        />
      </div>
    );
  }
}
export default FundAllocation;
