import React from "react";
import ReactDOM from "react-dom";
import logo from "./logo.svg";
import "./App.css";
import FundAllocation from "./FundAllocation.js";

function App() {
  return <FundAllocation />;
}

export default App;
