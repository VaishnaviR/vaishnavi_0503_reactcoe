import React from "react";
import "./App.css";
class FundCategory extends React.Component {
  constructor(props) {
    super();
  }
  createList = (parentIndex, treeData) => {
    if (treeData.length !== 0) {
      let list = [];
      for (let index in treeData) {
        let data = treeData[index];
        list.push(
          <div>
            <div className="column-flex ">
              <div className="row-flex">
                <div className="list-div">
                  {data.children.length > 0 && (
                    <span
                      className="triangle-down"
                      onClick={this.hideOrShow}
                    ></span>
                  )}
                  {data.children.length == 0 && (
                    <span className="indent"></span>
                  )}
                  <span className="text">{data.name}</span>
                </div>
                <div className="input-div">
                  <input
                    type="number"
                    value={parseInt(data.percentage).toFixed(2)}
                    onChange={e =>
                      this.props.onPercentageChange(
                        e.target.value,
                        parentIndex,
                        index
                      )
                    }
                  ></input>
                </div>
                <div className="amount-div">
                  <p>{data.amount.toFixed(2)}</p>
                </div>
              </div>
              <div className="nested">
                {this.createList(index, data.children)}
              </div>
            </div>
          </div>
        );
      }

      return list;
    }
  };
  render() {
    return (
      <div className="list-container">
        <div>{this.createList(-1, this.props.treeData)}</div>
      </div>
    );
  }
}

export default FundCategory;
